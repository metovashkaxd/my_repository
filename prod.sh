umask 0000
pwd
cd /var/www
mkdir $1
tar -xzvf arhiv.tar.gz -C $1 && echo "unzipped"
rm -rf /var/www/my_project
ln -s /var/www/$1 /var/www/my_project
cd my_project
php bin/console cache:clear
php bin/console cache:warmup
php bin/console doctrine:migrations:migrate